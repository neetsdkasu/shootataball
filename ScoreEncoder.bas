Attribute VB_Name = "ScoreEncoder"
Option Explicit

'特定桁までのスコア（点数、数字）を符号化する
'
'ScEncode( Long ) as String 　　符号化関数、但し数字に間違いがあるときECerrを返す
'ScDecode( String) as Long      復元関数、但し文字列に間違いがあるときDCerrを返す
'
'CODESIZE   (CODESIZE-1)桁までのスコアを符号化できる
'PSCODE     この値を0〜(4.5*10^(CODESIZE-1))の範囲の中の適当な整数で固定して使う
'
'使い方
'
'Dim st as String * 8  '符号化した文字列を格納する
'Dim score as Long     '符号化する点数
'
'st = ScEncode(score)  '変数stに変数scoreに格納されている数字を符号化した文字列を代入
'
'score = ScDecode(st)  '変数scoreに変数stに格納されている文字列から復元した数字を代入
'

Const CODESIZE As Integer = 6
Const PSCODE As Long = 387379
Const ECerr As String = ""
Const DCerr As Long = 0&

Private Function ChConvert(c As Integer) As String
    Dim r As String
    If Rnd() < 0.2! Then
        r = Chr(Asc("A") + c)
    ElseIf Rnd() < 0.25! Then
        r = Chr(Asc("A") + 10 + c)
    ElseIf Rnd() < 0.33! Then
        r = Chr(Asc("c") + c)
    ElseIf Rnd() < 0.5! Then
        r = Chr(Asc("c") + 10 + c)
    Else
        r = Format$(9 - c)
    End If
    ChConvert = r
End Function

Private Function NmConvert(c As Integer) As Integer
    Dim z As Integer
    If (c >= Asc("A")) And (c <= Asc("A") + 9) Then
        z = CInt(c - Asc("A"))
    ElseIf (c >= Asc("A") + 10) And (c <= Asc("A") + 19) Then
        z = CInt(c - Asc("A") - 10)
    ElseIf (c >= Asc("c")) And (c <= Asc("c") + 9) Then
        z = CInt(c - Asc("c"))
    ElseIf (c >= Asc("c") + 10) And (c <= Asc("c") + 19) Then
        z = CInt(c - Asc("c") - 10)
    Else
        z = 9 - CInt(Val(Chr$(c)))
    End If
    NmConvert = z
End Function

Public Function ScEncode(score As Long) As String
    Dim i As Integer, c As Integer
    Dim s As String, r As String, t As Integer
    Dim q As String, n As Integer, m As Integer, w As String
    Debug.Print "---ScEncode( "; score; " )---"
    n = Len(Format$(CODESIZE * 9)): m = 0: q = "": w = ""
    For i = 1 To n: m = m * 10 + 9: q = q + "0": Next i
    For i = 1 To CODESIZE: w = w + "0": Next i
    If (score < 0&) Or (Len(Format$(score)) >= CODESIZE) Then
        r = ECerr
        Debug.Print "Error"
    Else
        s = Format$(score + PSCODE, w)
        Debug.Print "score+PSCODE = "; s
        r = "": t = 0
        For i = 1 To CODESIZE
            c = CInt(Val(Mid$(s, i, 1)))
            t = t + c
        Next i
        Debug.Print "t = "; t, "m - t = "; m - t
        s = Format$(score + PSCODE + PSCODE, w)
        Debug.Print "score+pscode+pscode = "; s
        For i = 1 To CODESIZE
            c = CInt(Val(Mid$(s, i, 1)))
            r = ChConvert(c) + r
        Next i
        Debug.Print "r = "; r
        For i = 1 To n
            c = CInt(Val(Mid$(Format$(m - t, q), i, 1)))
            r = r + ChConvert(c)
        Next i
        Debug.Print "r = "; r
    End If
    ScEncode = r
    Debug.Print "---End of ScEncode---"
    Debug.Print
End Function

Public Function ScDecode(SCode As String) As Long
    Dim i As Integer, c As Integer, t As Integer
    Dim sc As Long, z As Integer, r As String, s As String
    Dim n As Integer, m As Integer, u As Integer, w As String
    Debug.Print "---ScDecode( "; SCode; " )---"
    n = Len(Format$(CODESIZE * 9)): m = 0: u = 0: w = ""
    For i = 1 To CODESIZE: w = w + "0": Next i
    If Len(SCode) = (CODESIZE + n) Then
        For i = 1 To n: m = m * 10 + 9: Next i
        r = ""
        For i = 1 To CODESIZE
            c = Asc(Mid$(SCode, i, 1))
            z = NmConvert(c)
            r = Format$(z) + r
        Next i
        Debug.Print "r = "; r
        s = Format$(CLng(Val(r)) - PSCODE, w)
        Debug.Print "s = "; s
        For i = 1 To CODESIZE
            c = CInt(Val(Mid$(s, i, 1)))
            t = t + c
        Next i
        Debug.Print "t = "; t
        For i = 1 To n
            c = Asc(Mid$(SCode, CODESIZE + i, 1))
            u = u * 10 + NmConvert(c)
        Next i
        z = m - u
        Debug.Print "z = "; z, "u = "; u
        If z = t Then
            sc = CLng(Val(r)) - PSCODE - PSCODE
            Debug.Print "sc = "; sc
        Else
            sc = DCerr
            Debug.Print "Error"
        End If
    Else
        sc = DCerr
        Debug.Print "Error"
    End If
    ScDecode = sc
    Debug.Print "---End of ScDecode---"
    Debug.Print
End Function
