Attribute VB_Name = "Win32API_BB"
Option Explicit

Public Declare Function BitBlt Lib "gdi32" ( _
    ByVal hDestDC As Long, _
    ByVal X As Long, _
    ByVal Y As Long, _
    ByVal nWidth As Long, _
    ByVal nHeight As Long, _
    ByVal hSrcDC As Long, _
    ByVal xSrc As Long, _
    ByVal ySrc As Long, _
    ByVal dwRop As Long) As Long
Public Const SRCAND = &H8800C6      'AND
Public Const SRCCOPY = &HCC0020     'COPY
Public Const SRCPAINT = &HEE0086    'OR

