Attribute VB_Name = "Win32API_sPS"
Option Explicit

Public Declare Function sndPlaySound Lib "winmm" _
    Alias "sndPlaySoundA" ( _
    ByVal lpszSound As String, _
    ByVal fuSound As Long) As Boolean

Public Const SND_SYNC As Long = &H0
Public Const SND_ASYNC As Long = &H1
Public Const SND_NODEFAULT As Long = &H2
Public Const SND_MEMORY As Long = &H4
Public Const SND_LOOP As Long = &H8
Public Const SND_NOSTOP As Long = &H10

